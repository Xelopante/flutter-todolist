import 'package:flutter/material.dart';
import 'package:todolist/models/task.dart';

/// == TASK_PREVIEW.DART ==
/// Widget qui construit la ListTile d'une task avec ses informations
class TaskPreview extends StatelessWidget {
  const TaskPreview({Key? key, required this.task, required this.onTaskTap})
      : super(key: key);

  /// Constructeur
  final Task task;

  /// CallBack envoyé à task_master quand on clique sur une tâche
  final Function onTaskTap;

  @override
  Widget build(BuildContext context) {
    return ListTile(
        title: Text(task.content),
        leading: !task.completed
            ? const Icon(Icons.check_box_outline_blank)
            : const Icon(Icons.check_box),
        onTap: () {
          onTaskTap(task);
        },
        shape: RoundedRectangleBorder(
            side: const BorderSide(
                color: Color.fromARGB(166, 77, 77, 77), width: 0.5),
            borderRadius: BorderRadius.circular(10)),
        tileColor: !task.completed
            ? const Color.fromRGBO(217, 108, 6, 0.8)
            : const Color.fromRGBO(189, 191, 9, 0.8));
  }
}
