import 'package:flutter/material.dart';
import 'package:todolist/components/tasks/task_preview.dart';
import 'package:todolist/models/task.dart';

/// == TASK_MASTER.DART ==
/// Widget qui construit une ListView de tâches via des ListTile
class TaskMaster extends StatelessWidget {
  const TaskMaster(
      {Key? key, required this.tasks, required this.onTaskPreviewTap})
      : super(key: key);

  /// Constructeur
  final List<Task> tasks;

  /// CallBack envoyé à all_tasks quand on clique sur une tâche
  final Function onTaskPreviewTap;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.separated(
        separatorBuilder: (BuildContext context, int index) {
          return const SizedBox(
            height: 5,
          );
        },
        padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
        itemCount: tasks.length,
        itemBuilder: (context, index) {
          /// Chaque ListTile de task est générée par le widget TaskPreview
          return TaskPreview(
              task: tasks[index],
              onTaskTap: (Task task) {
                onTaskPreviewTap(task);
              });
        },
      ),
    );
  }
}
