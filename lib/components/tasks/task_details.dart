import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todolist/models/task.dart';
import 'package:intl/intl.dart';
import 'package:todolist/models/tasks_collection.dart';
import 'package:todolist/screens/one_task.dart';

class TaskDetails extends StatelessWidget {
  const TaskDetails(
      {Key? key,
      this.task,
      required this.onTaskCloseTap,
      required this.onDeleteTap})
      : super(key: key);

  final Task? task;

  /// CallBack pour all_tasks pour fermer le taskDetail
  final Function onTaskCloseTap;

  /// CallBack pour all_tasks pour supprimer le taskDetail de la liste des données
  final Function onDeleteTap;

  /// Refactorisation de created_at en format propre
  String refactoDateTime(DateTime dateTime) {
    DateFormat dateFormat = DateFormat('dd-MM-yyyy - kk:mm');
    String dateRefactored = dateFormat.format(dateTime);

    return dateRefactored;
  }

  @override
  Widget build(BuildContext context) {
    /// Style des ElevatedButtons
    final ButtonStyle buttonStyle = ElevatedButton.styleFrom(
        primary: const Color.fromARGB(255, 49, 49, 49),
        textStyle: const TextStyle(fontSize: 15));

    return Container(
        color: const Color.fromARGB(204, 173, 173, 162),
        margin: const EdgeInsets.fromLTRB(0, 15, 0, 15),
        child: Column(children: <Widget>[
          Row(
            children: [
              Expanded(

                  /// ListTile qui contient les infos de la task
                  child: ListTile(
                      title: Center(child: Text(task!.content)),
                      subtitle: Center(
                          child: Text(
                              'Created : ' + refactoDateTime(task!.createdAt))),
                      onTap: () {
                        onTaskCloseTap(task);
                      },
                      tileColor: const Color.fromARGB(0, 0, 0, 0)))
            ],
          ),

          /// Élément qui contient les boutons pour intéragir avec la tâche
          Center(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ElevatedButton(
                  style: buttonStyle,
                  onPressed: () {
                    // Navigate to the second screen using a named route.
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Consumer<TasksCollection>(
                                  builder:
                                      (context, tasksCollectionRef, child) {
                                return OneTask(
                                  title: 'ToDo',
                                  tasksCollectionRef: tasksCollectionRef,
                                  task: task!,
                                );
                              })),
                    );
                  },
                  child: const Text('Update')),
              ElevatedButton(
                  style: buttonStyle,
                  onPressed: () {
                    onDeleteTap();
                  },
                  child: const Text('Delete'))
            ],
          ))
        ]));
  }
}
