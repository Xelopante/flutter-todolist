class Task {
  /// Attributs
  int id;
  String content;
  bool completed;
  DateTime createdAt;

  /// Constructeur
  Task(this.id, this.content, this.completed, this.createdAt);
}
