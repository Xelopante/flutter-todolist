import 'package:flutter/widgets.dart';
import 'package:todolist/data/tasks.dart' as data;
import 'package:todolist/models/task.dart';

class TasksCollection extends ChangeNotifier {
  void create(Task task) {
    data.taskList.add(task);

    notifyListeners();
  }

  void delete(Task task) {
    data.taskList.remove(task);

    notifyListeners();
  }

  void update(Task task) {
    int index = data.taskList.indexOf(task);
    data.taskList[index] = task;

    notifyListeners();
  }

  int length() {
    return data.taskList.length;
  }
}
