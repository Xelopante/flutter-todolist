import 'package:flutter/material.dart';
import 'package:todolist/models/tasks_collection.dart';
import 'package:todolist/screens/create_task.dart';
import 'package:todolist/screens/one_task.dart';
import 'screens/all_tasks.dart';
import 'package:provider/provider.dart';

/// == MAIN.DART ==
/// Fichier d'initialisation de l'application
void main() {
  runApp(
    /// On englobe l'app ToDo par le changeNotifier pour utiliser les consumers (provider package)
    ChangeNotifierProvider(
      create: (context) => TasksCollection(),
      child: const TodoList(),
    ),
  );
}

class TodoList extends StatelessWidget {
  const TodoList({Key? key}) : super(key: key);

  /// Retourne un widget stateless, "squelette" de l'application (thème, titre, ...)
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'ToDo List',
      routes: {
        '/all_tasks': (context) => Consumer<TasksCollection>(
                builder: (context, tasksCollectionRef, child) {
              return AllTasks(
                  title: 'ToDo', tasksCollectionRef: tasksCollectionRef);
            }),
        '/one_task': (context) => Consumer<TasksCollection>(
                builder: (context, tasksCollectionRef, child) {
              return OneTask(
                  title: 'ToDo', tasksCollectionRef: tasksCollectionRef);
            }),
        '/create_task': (context) => Consumer<TasksCollection>(
                builder: (context, tasksCollectionRef, child) {
              return CreateTask(
                  title: 'ToDo', tasksCollectionRef: tasksCollectionRef);
            }),
      },

      /// Appelle le widget AllTasks qui gère l'affichage de toutes les tasks (avec Consumer comme parent pour passer la ref de TasksCollection)
      home: Consumer<TasksCollection>(
          builder: (context, tasksCollectionRef, child) {
        return AllTasks(title: 'ToDo', tasksCollectionRef: tasksCollectionRef);
      }),

      /// Mise en place du thème de l'application (couleur des éléments comme SnackBar, Police d'écriture, ...)
      theme: ThemeData(
        brightness: Brightness.dark,
        fontFamily: 'Staarliches',
        snackBarTheme: const SnackBarThemeData(
          actionTextColor: Color.fromARGB(255, 255, 255, 255),
          contentTextStyle:
              TextStyle(color: Color.fromARGB(255, 255, 255, 255)),
          backgroundColor: Color.fromARGB(255, 49, 49, 49),
        ),
      ),
    );
  }
}
