import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todolist/models/task.dart';
import 'package:todolist/models/tasks_collection.dart';

/// == CREATE_TASK.DART ==
/// Widget "screen" qui gère la création d'une tâche (Retourne un Scaffold à main.dart)
class CreateTask extends StatefulWidget {
  const CreateTask(
      {Key? key, required this.title, required this.tasksCollectionRef})
      : super(key: key);

  /// Constructeur
  final String title;
  final TasksCollection tasksCollectionRef;

  @override
  State<CreateTask> createState() => _CreateTaskState();
}

class _CreateTaskState extends State<CreateTask> {
  String? taskContent;
  bool status = false;

  /// Supprime une tâche des données task (via le provider TasksCollection)
  void createTask() {
    /// Sachant que les ID de la TasksCollection sont gérés par incrémentation à partir de 1, on calcule l'ID de la nouvelle tâche par la taille du nombre de tâches existantes (= ID de la dernière) + 1
    int id = Provider.of<TasksCollection>(context, listen: false).length() + 1;

    Task task = Task(id, taskContent!, status, DateTime.now());

    Provider.of<TasksCollection>(context, listen: false).create(task);
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final ButtonStyle buttonStyle = ElevatedButton.styleFrom(
      primary: const Color.fromARGB(255, 67, 156, 179),
      textStyle: const TextStyle(fontSize: 15),
    );

    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(widget.title),
        ),

        /// Formulaire de création
        body: Container(
            margin: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Column(children: [
              Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      /// Vérification sur la valeur du texte
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter some text';
                        }

                        taskContent = value;
                      },
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Text("Status"),

                        /// Checkbox pour le status de la tâche
                        Checkbox(
                          checkColor: Colors.white,
                          value: status,
                          onChanged: (bool? value) {
                            setState(() {
                              status = value!;
                            });
                          },
                        ),
                      ],
                    ),
                    ElevatedButton(
                      style: buttonStyle,
                      onPressed: () {
                        /// Si les inputs du form de validation sont corrects, on crée la tâche puis redirection avec snackbar
                        if (_formKey.currentState!.validate()) {
                          createTask();
                          ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                                content: Text('Task successfully created')),
                          );

                          Navigator.pushNamed(context, '/all_tasks');
                        }
                      },
                      child: const Text('Submit'),
                    ),
                  ],
                ),
              ),
            ])));
  }
}
