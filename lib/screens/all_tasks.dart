import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todolist/components/tasks/task_master.dart';
import 'package:todolist/models/task.dart';
import 'package:todolist/data/tasks.dart' as data;
import 'package:todolist/components/tasks/task_details.dart';
import 'package:todolist/models/tasks_collection.dart';
import 'package:todolist/screens/create_task.dart';

/// == ALL_TASKS.DART ==
/// Widget "screen" qui gère l'affichage de toutes les tâches (Retourne un Scaffold à main.dart)
class AllTasks extends StatefulWidget {
  const AllTasks(
      {Key? key, required this.title, required this.tasksCollectionRef})
      : super(key: key);

  /// Constructeur
  final String title;
  final TasksCollection tasksCollectionRef;

  @override
  State<AllTasks> createState() => _AllTasksState();
}

class _AllTasksState extends State<AllTasks> {
  Task? selectedTask;
  bool tapped = false;

  void closeTask() {
    selectedTask = null;
  }

  /// Supprime une tâche des données task (via le provider TasksCollection)
  void deleteTask() {
    Provider.of<TasksCollection>(context, listen: false).delete(selectedTask!);
  }

  @override
  Widget build(BuildContext context) {
    /// SnackBar de confirmation de suppression de tâche
    final snackBar = SnackBar(
      content: const Text('Are you sure you want to delete that task ?'),
      action: SnackBarAction(
        label: 'Confirm',
        onPressed: () {
          setState(() {
            deleteTask();
            closeTask();
          });
        },
      ),
    );

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.title),
      ),

      /// Appelle le widget TaskMaster qui gère la liste de toutes les tâches
      body: Column(
        children: [
          selectedTask != null
              ? TaskDetails(
                  task: selectedTask,

                  /// Si on clique sur le TaskDetail, il se ferme
                  onTaskCloseTap: (selectedTask) {
                    setState(() {
                      closeTask();
                    });
                  },

                  /// Si on appuie sur "Delete", on affiche la SnackBar pour demander confirmation
                  onDeleteTap: () {
                    setState(() {
                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                    });
                  },
                )
              : Container(),
          TaskMaster(
            tasks: data.taskList,
            onTaskPreviewTap: (Task task) {
              /// Si on clique sur la ListTile sur laquelle on a appuyé (donc la tâche qu'on a demandé à afficher), on ferme le TaskDetail
              if (selectedTask != null && task.id == selectedTask!.id) {
                setState(() {
                  closeTask();
                });
              } else {
                /// Si la ListTile cliquée est pas celle cliquée précédemment, on affiche le TaskDetail
                setState(() {
                  selectedTask = task;
                });
              }
            },
          ),
        ],
      ),

      /// Bouton de redirection vers la page de création de Task
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => Consumer<TasksCollection>(
                        builder: (context, tasksCollectionRef, child) {
                      return CreateTask(
                          title: 'Create Task',
                          tasksCollectionRef: tasksCollectionRef);
                    })),
          );
        },
        backgroundColor: Colors.white,
        child: const Icon(Icons.add),
      ),
    );
  }
}
