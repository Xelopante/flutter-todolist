import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todolist/models/task.dart';
import 'package:todolist/models/tasks_collection.dart';

/// == ONE_TASK.DART ==
/// Widget "screen" qui gère l'affichage d'une seule tâche et permet de la modifier (Retourne un Scaffold à main.dart)
class OneTask extends StatefulWidget {
  const OneTask(
      {Key? key,
      required this.title,
      required this.tasksCollectionRef,
      this.task})
      : super(key: key);

  /// Constructeur
  final String title;
  final TasksCollection tasksCollectionRef;
  final Task? task;

  @override
  State<OneTask> createState() => _OneTaskState();
}

class _OneTaskState extends State<OneTask> {
  /// Modifie une tâche existante
  void updateTask() {
    Provider.of<TasksCollection>(context, listen: false).update(widget.task!);
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final ButtonStyle buttonStyle = ElevatedButton.styleFrom(
      primary: const Color.fromARGB(255, 67, 156, 179),
      textStyle: const TextStyle(fontSize: 15),
    );

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.title),
      ),
      body: Container(
          margin: const EdgeInsets.fromLTRB(5, 5, 5, 5),
          child: Column(children: [
            /// Formulaire de création
            Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  TextFormField(
                    /// Vérification sur la valeur du texte
                    initialValue: widget.task!.content,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter some text';
                      }

                      widget.task!.content = value;
                    },
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Text("Status"),

                      /// Checkbox pour le status de la tâche
                      Checkbox(
                        checkColor: Colors.white,
                        value: widget.task!.completed,
                        onChanged: (bool? value) {
                          setState(() {
                            widget.task!.completed = value!;
                          });
                        },
                      ),
                    ],
                  ),
                  ElevatedButton(
                    style: buttonStyle,
                    onPressed: () {
                      /// Si les inputs du form de validation sont corrects, on modifie la tâche puis redirection avec snackbar
                      if (_formKey.currentState!.validate()) {
                        updateTask();
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                              content: Text('Task successfully updated')),
                        );

                        Navigator.pushNamed(context, '/all_tasks');
                      }
                    },
                    child: const Text('Submit'),
                  ),
                ],
              ),
            ),
          ])),
    );
  }
}
