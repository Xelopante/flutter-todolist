/// Package faker : génération de string/nombres/dates aléatoires ...
import 'package:faker/faker.dart';
import 'package:todolist/models/task.dart';

/// == TASKS.DART ==
/// Génère une liste d'objets 'Task' avec des attributs remplis aléatoirement

var faker = Faker();

var taskList = List<Task>.generate(
    15,
    (i) => Task(i, faker.lorem.sentence(), random.boolean(),
        faker.date.dateTime(minYear: 2022, maxYear: 2023)));
